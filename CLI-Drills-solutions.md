1. Create the following directory structure. (Create empty files where necessary)

```
 hello
 ├── five
 │   └── six
 │       ├── c.txt//
 │       └── seven
 │           └── error.log
 └── one
     ├── a.txt
     ├── b.txt
     └── two
         ├── d.txt
         └── three
             ├── e.txt
             └── four
                 └── access.log
```


*  mkdir -p hello/five/six/seven/ 
*  mkdir -p hello/one/two/three/four/
*  touch hello/five/six/seven/error.log
*  touch hello/five/six/c.txt
*  touch hello/one/two/three/four/access.log
*  touch hello/one/two/three/e.txt
*  touch hello/one/two/d.txt
*  touch hello/one/a.txt
*  touch hello/one/b.txt

2. Delete all the files with the .log extension

```
rm hello/one/two/three/four/access.log
rm hello/five/six/seven/error.log
```

3. Add the following content to a.txt
  
`echo "Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others." >> hello/one/a.txt`

4. Delete the directory named five.

`rm -rf five/`

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------


# Drills Part 2

## Harry Potter

1. Download the contents of "Harry Potter and the Goblet of fire" using the command line from https://raw.githubusercontent.com/bobdeng/owlreader/master/ERead/assets/books/Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt

```
wget "https://raw.githubusercontent.com/bobdeng/owlreader/master/ERead/assets/books/Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt"
```
2. Print the first three lines in the book

```
head -3 "Harry Potter and the Goblet of Fire.txt"
```

3. Print the last 10 lines in the book

```
tail "Harry Potter and the Goblet of Fire.txt"
```

Use pipes to solve the following problems:

Refer 

* https://swcarpentry.github.io/shell-novice/04-pipefilter/index.html
* https://ryanstutorials.net/linuxtutorial/piping.php
* https://www.geeksforgeeks.org/piping-in-unix-or-linux/

to learn about pipes

4. How many times do the following words occur in the book?
    * Harry
    ```
      grep -o -i Harry "Harry Potter and the Goblet of Fire.txt" | wc -l
      3172
    ```
    * Ron
    ```
      grep -o -i Ron "Harry Potter and the Goblet of Fire.txt" | wc -l
      1358
    ```
    * Hermione
    ```
      grep -o -i Hermione "Harry Potter and the Goblet of Fire.txt" | wc -l
      872
    ```
    * Dumbledore
    ```
      grep -o -i Dumbledore "Harry Potter and the Goblet of Fire.txt" | wc -l
      596
    ```


5. Print lines from 100 through 200 in the book
```
head -200 "Harry Potter and the Goblet of Fire.txt" | tail -101
```
6. How many unique words are present in the book?
```
cat "Harry Potter and the Goblet of Fire.txt"| sed -e 's/[!:";,()'\'']/ /g;s/  */ /g'|tr "\n" " " |tr '\t' ' '|tr -s ' '| tr ' ' '\n' | sort | uniq | wc -l
15902
```
___________

## Processes

1. List your browser's process ids (pid) and parent process ids(ppid)
```
pidof chrome
```
2. Stop the browser application from the command line
```
kill -9 $(pidof firefox)

```
3. List the top 3 processes by CPU usage.
```
ps aux | sort -nrk 3,3 | head -n 3
```
____________

## Managing software

Use `apt` (Ubuntu) or `homebrew` (Mac) to:


1. Install `git`, `vim` and `nginx`
```
sudo apt-get install git
sudo apt-get install vim
sudo apt-get install nginx
```
2. Uninstall `nginx`
_____________
```
 
sudo apt-get remove nginx nginx-commom
or
sudo apt-get purge nginx nginx-commom
```

## Misc

1. What's your local ip address?
```
hostname -I
```
2. Find the ip address of `google.com`
```
host www.google.com
```
3. Where is the `python` command located? What about `python3`?
```
whereis python and whereis python3
```

